MIDNIGHT for Recalbox
--------------------------------------------------------------------
Version : 2.6 - 18/05/2024   
Author : Bounitos

## Compatibility :
- Recalbox >= 9.2
- HDMi HD
- CRT / JAMMA 240/480p
- TATE

## Download
Lastest Version  
Version 2.6 : [Theme-Midnight-last.zip](https://www.bounar.fr/Themes_es/Midnight/Theme-Midnight-last.zip)

Older version for RGBDual Only and Recalbox 9.1  
Version 1.5 : [retrobase-240p_1.5.zip](https://www.bounar.fr/Themes_es/Retrobase240p/retrobase-240p_1.5.zip)


## Configuration

### Résolution
// To avoid performance problems: lag and flashing  
// Pour éviter des problèmes de performance : lags et clignotements
- Pi4 : 1280 x 1080px
- Pi5 : 1920 x 1080px
- x86 : 1920 x 1080px

### Screen Scraper
- Image type : BOX2D
- Video type : Optimized
- Thumbnail type : Logo

### Choice of shader / Choix du shader
- Menu > UI option > Theme Configuration
- Choose option

### Choice of Clips game  / Choix de l'affichage de l'ecran de veille "Clip de jeux"
- Menu > UI option > Theme Configuration > Theme clip
- Choose option

## Screenshot HD - 240/480p

### SystemView HD (Shader options : no, crt, nid d'abeille, scanline) :
**_no shader_**   
![Systemeview](/_assets/img/screen/Systemview_V_no-shader.png)  
**_CRT shader_**  
![Systemeview](/_assets/img/screen/Systemview_V_crt-shader.png)  
**_Nid d'abeille shader_**  
![Systemeview](/_assets/img/screen/Systemview_V_nidabeille-shader.png)  
**_Scanlines shader_**    
![Systemeview](/_assets/img/screen/Systemview_V_scanline-shader.png)

### SystemView 240/480p :
![Systemeview](/_assets/img/screen/Systemview_Sega-saturn.png)
![Systemeview](/_assets/img/screen/Systemview_Vectrex.png)

### SystemView TATE :
![Systemeview](/_assets/img/screen/Systemview_TATE_HD.png)

### Gamelistview HDMI HD :
![Gamelistview](/_assets/img/screen/gamelist1_HD.png)

### Gamelistview CRT-JAMMA - 240/480p:
![Gamelistview](/_assets/img/screen/Gamelist2.png)
![Gamelistview](/_assets/img/screen/Gamelist_7px.png)

### Gamelistview TATE CRT/HD :
![Gamelistview](/_assets/img/screen/Gamelist_TATE_CRT_2.png)
![Gamelistview](/_assets/img/screen/Gamelist_TATE_CRT.png)
![Gamelistview](/_assets/img/screen/Gamelist_TATE_HD.png)

### Menu :
![Menu](/_assets/img/screen/Menu.png)


## Changelog
```
18/05/2024 : Version 2.6 
- Add new arcade virtal system (raizing)
```
```
10/04/2024 : Version 2.5 
- Name Change (Retrobase -> Midnight)
- New gamelistview for CRT TATE""
- Add new arcade systeme
- Fix for Tate left
```
```
20/03/2024 : Version 2.4 
- Fix logo, gameinfo for overscan CRT
- Add Jamma button for helpbar
```
```
18/03/2024 : Version 2.3  
- Gameclip view Full video no_infos
- Add compatibily with TATE mode (9:16 & 3:4)
- Systemes backgound
- Fix bug flag on listgame
- Resising backgrounds for fix TATE RIGHT bug 
- Fix ratio videoclip on gamelist for tate games
```
```
15/02/2024 : Version 2.2  
- Gameclip view
- Fix  developer info in CRT Gamelist
- Oric + Creativison Background
```
```
14/02/2024 : Version 2.1  
- Fix Gamelist crash
```
```
06/02/2024 : Version 2.0  
- Compatibility recalbox 9.2
- Compatibility HD with  shader overlay
```
```
22/01/2024 : Version 1.5  
- Fix Tvalidation buttons colors
- Helpbar left ajustement fort CRT with overscan
```
```
10/01/2024 : Version 1.4  
- Fix TATE background + logo
- Add missing logos (systems + virtual systems)
```
```
09/01/2024 : Version 1.3  
- Fontsize menu fix - pixel perfect
- Gamelist align left for better compatibility (flag + arcade system view)
- Add Logos (Naomigd, Screenshot, PCengine CD)
```
```
29/12/2023 : Version 1.2  
- Fontsize fix
- Add Theme option to change the Gamelistview Fontsize (menu >  interface option > Theme configuration)
- Add Arcade system backgrounds
- Add Virtual games type Logos
- Add Virtual games type backgrounds
```
```
19/12/2023 - Version : 1.1 
- Init
```


## Licence
<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://www.bounar.fr/Themes_es/Midnight/">Midnight (Recalbox theme)</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://www.linkedin.com/in/benoit-bounar-157b2933/">Benoit BOUNAR</a> is licensed under <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1" alt=""></a></p>

